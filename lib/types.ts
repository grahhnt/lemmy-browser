export type ICommunity = {
  /**
   * <name>@<hostname>
   */
  _id: string;

  title: string;
  description: string | null;
  tags: string[];
  subreddits: string[];
  languages: number[] | undefined;

  actor: string;

  icon: string | null;
  banner: string | null;

  stats: {
    subscribers: number;
    posts: number;
    users: {
      today: number;
      week: number;
      month: number;
      half_year: number;
    };
  };

  flags: {
    nsfw: boolean;
    only_mod_posts: boolean;
    hidden: boolean;

    /**
     * Pin at the top (most likely going to be removed)
     */
    feature: boolean;
  };

  lastCrawl?: Date;
  _createdAt?: Date;
  _updatedAt?: Date;
};

export type IInstance = {
  hostname: string;

  name: string;
  description: string | null;

  icon: string | null;
  banner: string | null;

  flags: {
    downvotes: boolean;
    allow_nsfw: boolean;
    limit_community_creation: boolean;
    has_application_question: boolean;
    registration_mode: "open" | "closed" | "requireapplication";

    private: boolean;
    federated: boolean;
  };

  stats: {
    communities: number;
  };

  lastCrawl?: Date;
  _createdAt?: Date;
  _updatedAt?: Date;
};

export type LemmyLanguage = {
  id: number;
  code: string;
  name: string;
};
