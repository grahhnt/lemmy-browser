import mongoose from "mongoose";
import { IInstance } from "../types";

const InstanceSchema = new mongoose.Schema<IInstance>(
  {
    hostname: String,

    name: String,
    description: String,

    icon: String,
    banner: String,

    flags: {
      downvotes: Boolean,
      allow_nsfw: Boolean,
      limit_community_creation: Boolean,
      has_application_question: Boolean,
      registration_mode: String,

      private: Boolean,
      federated: Boolean,
    },

    stats: {
      communities: Number,
    },

    lastCrawl: Date,
  },
  {
    timestamps: {
      createdAt: "_createdAt",
      updatedAt: "_updatedAt",
    },
  }
);

export default (mongoose.models.Instance ||
  mongoose.model("Instance", InstanceSchema)) as mongoose.Model<IInstance>;
