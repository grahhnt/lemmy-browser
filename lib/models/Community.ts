import mongoose from "mongoose";
import { ICommunity } from "../types";

const CommunitySchema = new mongoose.Schema<ICommunity>(
  {
    _id: String,
    title: String,
    description: String,
    tags: [String],
    subreddits: [String],
    languages: [Number],

    actor: String,

    icon: String,
    banner: String,

    stats: {
      subscribers: Number,
      posts: Number,
      users: {
        today: Number,
        week: Number,
        month: Number,
        half_year: Number,
      },
    },

    flags: {
      nsfw: Boolean,
      only_mod_posts: Boolean,
      hidden: Boolean,
      feature: Boolean,
    },

    lastCrawl: Date,
  },
  {
    timestamps: {
      createdAt: "_createdAt",
      updatedAt: "_updatedAt",
    },
  }
).index({
  title: "text",
  description: "text",
});

export default (mongoose.models.Community ||
  mongoose.model("Community", CommunitySchema)) as mongoose.Model<ICommunity>;
