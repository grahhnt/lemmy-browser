import { BareFetcher } from "swr";

export const swr_fetcher: BareFetcher<any> = async (resource, init) => {
  const req = await fetch(resource, init);

  if (req.status !== 200) {
    const err = new Error(`Endpoint responded with ${req.status}`);

    // @ts-ignore
    err.status = req.status;

    throw err;
  }

  let json = await req.json();

  if (typeof json.success === "boolean" && !json.success) {
    throw new Error(json.error || json.errors.join(", ") || "json failure");
  }

  if (typeof json.data !== "undefined") {
    return json.data;
  }

  return json;
};
