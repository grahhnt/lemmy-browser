import path from "path";
import { RedisClientType, createClient } from "redis";

if (process.env.NODE_ENV !== "production") {
  require("dotenv").config({ path: path.join(__dirname, "../.env.local") });
}

const { REDIS_URI, MONGODB_URI, HOMESERVER } = process.env;
const REDIS_PREFIX = "lemmy-spider";
const TAG_REGEX = /(^|\s)(#[a-zA-Z0-9_]{1,})($|\b)/gm;
const SUBREDDIT_REGEX = /(^|\s)(\/?r\/[a-zA-Z0-9_]{1,})($|\b)/gm;
const MAX_COMMUNITIES_PER_INSTANCE =
  !process.env.MAX_COMMUNITIES_PER_INSTANCE ||
  isNaN(parseInt(process.env.MAX_COMMUNITIES_PER_INSTANCE))
    ? 20
    : parseInt(process.env.MAX_COMMUNITIES_PER_INSTANCE);

console.log("Configuration", {
  HOMESERVER,
  REDIS_PREFIX,
  MAX_COMMUNITIES_PER_INSTANCE,
});

if (!REDIS_URI) {
  console.error("Missing REDIS_URI environment variable (../.env.local)");
  process.exit(1);
}

if (!MONGODB_URI) {
  console.error("Missing MONGODB_URI environment variable (../.env.local)");
  process.exit(1);
}

import dbConnect from "../lib/mongoose";
import Instance from "../lib/models/Instance";
import Community from "../lib/models/Community";

const run = async () => {
  const client = createClient({
    url: REDIS_URI,
  });

  console.info("Connecting to redis...");
  await client.connect();
  console.info("Connected to redis");
  console.info("Connecting to mongodb...");
  await dbConnect();
  console.info("Connected to mongodb");

  enum CrawlResult {
    QUEUE_EMPTY = "queue empty",
    FINISHED = "finished",
    ERROR = "error",
    UNSUPPORTED_SOFTWARE = "unsupported software",
  }

  const crawl = async (
    server: string
  ): Promise<{
    result: CrawlResult;
    message?: string;
  }> => {
    console.info(`Crawling ${server}`);

    let software = await getServerInfo(server);

    console.info(
      `${server} is running ${software.software} (${software.version})`
    );

    switch (software.software) {
      case "lemmy": {
        const ENDPOINT = `https://${server}/api/v3`;
        try {
          const siteinfo = (
            await safe_fetch(ENDPOINT + "/site", async (req) => {
              if (req.status !== 200)
                throw new Error(
                  `${server} siteinfo is not reachable [returned ${req.status}]`
                );

              try {
                return await req.json();
              } catch (e) {
                throw new Error(
                  `${server} siteinfo is not reachable [returned invalid json]`
                );
              }
            })
          )?.site_view;

          console.info(`${server} saving instance...`);
          const instance = await Instance.findOneAndUpdate(
            { hostname: server },
            {
              $set: {
                hostname: server,
                name: siteinfo.site.name,
                description: siteinfo.site.description,
                icon: siteinfo.site.icon,
                banner: siteinfo.site.banner,
                flags: {
                  downvotes: siteinfo.local_site.enable_downvotes,
                  allow_nsfw: siteinfo.local_site.enable_nsfw,
                  limit_community_creation:
                    siteinfo.local_site.community_creation_admin_only,
                  has_application_question:
                    !!siteinfo.local_site.application_question,
                  registration_mode: siteinfo.local_site.registration_mode,
                  private: siteinfo.local_site.private_instance,
                  federated: siteinfo.local_site.federation_enabled,
                },
                stats: {
                  communities: siteinfo.counts.communities,
                },
                lastCrawl: new Date(),
              },
            },
            { upsert: true, new: true }
          );
          console.info(`${server} saved instance`);

          let communitiesLoaded = 0;

          const fetchCommunities = async (page: number) => {
            console.info(`${server} communities page ${page}`);
            const communities = await safe_fetch(
              `${ENDPOINT}/community/list?type_=Local&sort=Active&limit=10&page=${page}`,
              async (req) => {
                if (req.status !== 200)
                  throw new Error(
                    `${server} communities is not reachable [returned ${communities.status}]`
                  );

                try {
                  return await req.json();
                } catch (e) {
                  throw new Error(
                    `${server} communities is not reachable [returned invalid json]`
                  );
                }
              }
            );

            if (!communities?.communities) {
              throw new Error(
                `${server} communities did not return communities`
              );
            }

            if (communities.communities.length === 0) {
              console.info(`${server} loaded ${communitiesLoaded} communities`);
              return;
            }

            for (let community of communities.communities) {
              const _id = `${community.community.name}@${server}`;

              const communityDetails: Response = await safe_fetch(
                `${ENDPOINT}/community?id=${community.community.id}`
              );
              let languages: number[] | undefined;

              if (communityDetails.status !== 200) {
                console.info(
                  `${server}/${community.community.name} didn't respond [${communityDetails.status}]`
                );
              } else {
                let communityDetailsj;

                try {
                  communityDetailsj = await communityDetails.json();
                } catch (e) {
                  console.info(
                    `${server}/${community.community.name} didn't give json`
                  );
                }

                if (communityDetailsj?.discussion_languages.length > 0) {
                  languages = communityDetailsj.discussion_languages;
                }
              }

              const tags = (
                (community.community.description as string)?.match(TAG_REGEX) ||
                []
              )
                .slice(0, 5)
                .map((a) => a.trim());
              const subreddits = (
                (community.community.description as string)?.match(
                  SUBREDDIT_REGEX
                ) || []
              )
                .slice(0, 5)
                .map((a) => a.trim());

              await Community.findOneAndUpdate(
                { _id },
                {
                  $set: {
                    _id,
                    title: community.community.title,
                    description: community.community.description,
                    tags,
                    subreddits,
                    languages,
                    actor: community.community.actor_id,

                    icon: community.community.icon,
                    banner: community.community.banner,

                    stats: {
                      subscribers: community.counts.subscribers,
                      posts: community.counts.posts,
                      users: {
                        today: community.counts.users_active_day,
                        week: community.counts.users_active_week,
                        month: community.counts.users_active_month,
                        half_year: community.counts.users_active_half_year,
                      },
                    },

                    flags: {
                      nsfw: community.community.nsfw,
                      only_mod_posts:
                        community.community.posting_restricted_to_mods,
                      hidden: community.community.hidden,
                      feature: _id === "browse@toast.ooo",
                    },

                    lastCrawl: new Date(),
                  },
                },
                { upsert: true }
              );

              communitiesLoaded++;
            }

            if (
              communitiesLoaded < MAX_COMMUNITIES_PER_INSTANCE &&
              communities.communities.length === 10
            ) {
              if (exitMode === ExitMode.KILL_COMMUNITY_BROWSE) {
                console.warn("Stopping community browse...");
              } else {
                await fetchCommunities(page + 1);
              }
            } else {
              console.info(`${server} loaded ${communitiesLoaded} communities`);

              // if (servers.indexOf(server) + 1 >= servers.length) {
              //   progress = 1;
              //   progressBar.update(progress);
              // }
            }
          };

          if (instance.stats.communities > 0) {
            await fetchCommunities(1);
          } else {
            console.info(`${server} has no communities`);
          }

          return {
            result: CrawlResult.FINISHED,
          };
        } catch (e: any) {
          return {
            result: CrawlResult.ERROR,
            message: e?.message || e,
          };
        }
      }
      default:
        return {
          result: CrawlResult.UNSUPPORTED_SOFTWARE,
          message: `${server} is running ${software.software}, which isn't supported`,
        };
    }
  };

  enum ExitMode {
    RUNNING,
    KILL_QUEUE,
    KILL_COMMUNITY_BROWSE,
    FORCE_KILL,
  }
  let exitMode = ExitMode.RUNNING;
  const handleExit = () => {
    switch (exitMode) {
      case ExitMode.RUNNING:
        exitMode = ExitMode.KILL_QUEUE;
        console.info(
          "[^C] Stopping after this instance... (ctrl-c -> kill community page)"
        );
        break;
      case ExitMode.KILL_QUEUE:
        exitMode = ExitMode.KILL_COMMUNITY_BROWSE;
        console.info(
          "[^C] Stopping after community page... (ctrl-c -> force kill)"
        );
        break;
      case ExitMode.FORCE_KILL:
        console.warn("[^C] Force killing...");
        process.exit();
    }
  };

  switch (process.argv[2]) {
    case "build":
      console.info(`Fetching federated with from ${HOMESERVER}`);
      const federatedWith = (
        await fetch(`https://${HOMESERVER}/api/v3/site`).then((a) => a.json())
      )?.federated_instances.linked;

      if (!federatedWith || !federatedWith.length) {
        console.error(
          `${HOMESERVER} has no federated instances (or inaccessable)`
        );
        process.exit(1);
      }

      await client.del([REDIS_PREFIX, "instance-queue"].join(":"));
      await client.lPush(
        [REDIS_PREFIX, "instance-queue"].join(":"),
        federatedWith as string[]
      );
      console.info(`Added ${federatedWith.length} servers to the queue`);
      process.exit(0);
    case "crawl":
      console.info("Starting crawl...");
      let targetServer: string | undefined = undefined;
      let shouldUseQueue = true;

      if (process.argv[3]) {
        targetServer = process.argv[3];
        shouldUseQueue = false;
      }

      process.on("SIGINT", handleExit);

      let ret: Awaited<ReturnType<typeof crawl>>;
      do {
        //@ts-ignore
        if (typeof ret !== "undefined") {
          console.info("Previous crawl output", ret);
        }
        if (exitMode !== ExitMode.RUNNING) {
          console.log("Stopping crawl");
          break;
        }

        try {
          let server = targetServer;

          if (!shouldUseQueue && !server) {
            ret = {
              result: CrawlResult.QUEUE_EMPTY,
            };
            break;
          }

          if (shouldUseQueue && !server) {
            server =
              (await client.lPop([REDIS_PREFIX, "instance-queue"].join(":"))) ||
              undefined;

            if (!server) {
              ret = {
                result: CrawlResult.QUEUE_EMPTY,
              };
              break;
            }
          }

          ret = await crawl(server!);

          if (!shouldUseQueue) {
            targetServer = undefined;
          }
        } catch (e: any) {
          console.error("Uncaught Crawl Error", e);
          ret = {
            result: CrawlResult.ERROR,
            message: e?.message || e,
          };
        }
      } while (ret.result !== CrawlResult.QUEUE_EMPTY);

      if (exitMode !== ExitMode.RUNNING) console.log("Queue empty");

      process.off("SIGINT", handleExit);

      process.exit(0);
    default:
      console.error("Unknown option");
      process.exit(1);
  }
};

const safe_fetch = async (
  url: string,
  pre: (req: Response) => Promise<any> = (req) => Promise.resolve(req)
) => {
  const req = await fetch(url, {
    headers: {
      "User-Agent": "lemmy-browser-spider/1.0 (https://toast.ooo/c/browse)",
    },
  });

  return await pre(req);
};

const getServerInfo = async (
  server: string
): Promise<{
  software: string;
  version: string;
}> => {
  const nodeinfo = await safe_fetch(`https://${server}/.well-known/nodeinfo`);

  if (nodeinfo.status !== 200) {
    throw new Error(`${server} is not a service [nodeinfo ${nodeinfo.status}]`);
  }

  let nodeinfoj;

  try {
    nodeinfoj = await nodeinfo.json();
  } catch (e) {
    throw new Error(`${server} has a non-json nodeinfo`);
  }

  let nodeinfo_link = nodeinfoj.links?.find(
    (l: any) => l.rel === "http://nodeinfo.diaspora.software/ns/schema/2.0"
  );

  if (!nodeinfo_link || !nodeinfo_link.href) {
    throw new Error(`${server} doesn't have a nodeinfo v2`);
  }

  const nodeinfov2 = await safe_fetch(nodeinfo_link.href);

  if (nodeinfov2.status !== 200) {
    throw new Error(
      `${server} is not reachable [nodeinfo v2 returned ${nodeinfo.status}]`
    );
  }

  let nodeinfov2j;

  try {
    nodeinfov2j = await nodeinfov2.json();
  } catch (e) {
    throw new Error(
      `${server} has a non-json nodeinfo v2 [${nodeinfo_link.href}]`
    );
  }

  if (!nodeinfov2j?.software?.name) {
    throw new Error(`${server} doesn't have a software name`);
  }

  return {
    software: nodeinfov2j.software.name.toLowerCase(),
    version: nodeinfov2j.software.version,
  };
};

run();
