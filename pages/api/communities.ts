import Community from "@/lib/models/Community";
import dbConnect from "@/lib/mongoose";
import { NextApiRequest, NextApiResponse } from "next";

export default async function CommunitiesAPI(
  req: NextApiRequest,
  res: NextApiResponse
) {
  await dbConnect();
  let options: {
    limit: number;
    page: number;
    search?: string;
    filters?: string[];
  } = {
    limit: 10,
    page: 0,
  };
  let notes: string[] = [];

  if (req.query.limit && typeof req.query.limit === "string") {
    options.limit = parseInt(req.query.limit);

    if (isNaN(options.limit)) {
      options.limit = 10;
      notes.push("?limit is not a number -- defaulted to 10");
    }
  }

  if (options.limit < 1 || options.limit > 30) {
    return res.json({
      success: false,
      errors: ["?limit has a minimum of 1 and a maxium of 30"],
    });
  }

  if (req.query.page && typeof req.query.page === "string") {
    options.page = parseInt(req.query.page);

    if (isNaN(options.page)) {
      options.page = 0;
      notes.push("?page is not a number -- defaulted to 0");
    }
  }

  if (options.page < 0) {
    return res.json({
      success: false,
      errors: ["?page has a minimum of 0"],
    });
  }

  if (req.query.search && typeof req.query.search === "string") {
    options.search = req.query.search;
  }

  if (req.query.filters && typeof req.query.filters === "string") {
    options.filters = req.query.filters.split(",");
  }

  let hasPrev = false;
  let hasNext = false;

  let query: any = [];
  let hideNSFW = true;

  if (options.filters && options.filters.length > 0) {
    options.filters.forEach((filter) => {
      if (filter.indexOf(":") === -1) {
        query.push({
          tags: filter,
        });
      } else {
        let mode = filter.split(":")[0];
        let val = filter.split(":")[1];
        switch (mode) {
          case "lang":
            query.push({
              languages: parseInt(val),
            });
            break;
          case "nsfw":
            if (val === "show") {
              hideNSFW = false;
            }
            if (val === "only") {
              hideNSFW = false;
              query.push({
                "flags.nsfw": true,
              });
            }
            break;
          case "subreddit":
            query.push({
              subreddits: val,
            });
            break;
        }
      }
    });
  }

  if (hideNSFW) {
    query.push({
      "flags.nsfw": false,
    });
  }

  if (options.search) {
    query.push({
      $text: {
        $search: options.search,
      },
    });
  }

  const effectiveQuery = query.length > 0 ? { $and: query } : {};

  const totalCommunities = await Community.find(effectiveQuery, {
    _id: 1,
  }).count();

  const communities = await Community.find(effectiveQuery)
    .sort({
      "stats.subscribers": -1,
    })
    .limit(options.limit)
    .skip(options.limit * options.page);

  if (options.page > 0) {
    hasPrev = true;
  }

  const nextPage = await Community.find(effectiveQuery, { _id: 1 })
    .limit(options.limit)
    .skip(options.limit * (options.page + 1))
    .count();
  if (nextPage > 0) {
    hasNext = true;
  }

  res.json({
    success: true,
    data: {
      meta: {
        notes,
        total: totalCommunities,
        hasPrev,
        hasNext,
      },
      communities,
    },
  });
}
