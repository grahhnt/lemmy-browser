import dbConnect from "@/lib/mongoose";
import { NextApiRequest, NextApiResponse } from "next";
import Community from "@/lib/models/Community";

export default async function FiltersEndpoint(
  req: NextApiRequest,
  res: NextApiResponse
) {
  await dbConnect();

  const tags = await Community.aggregate([
    { $unwind: "$tags" },
    {
      $group: {
        _id: "$tags",
        communities: { $addToSet: "$_id" },
      },
    },
    { $project: { _id: 1, uses: { $size: "$communities" } } },
    { $sort: { uses: -1, _id: 1 } },
  ]);

  const languages = await Community.aggregate([
    { $unwind: "$languages" },
    {
      $group: {
        _id: "$languages",
        communities: { $addToSet: "$_id" },
      },
    },
    { $project: { _id: 1, uses: { $size: "$communities" } } },
    { $sort: { uses: -1 } },
  ]);

  res.json({
    success: true,
    data: { tags, languages },
  });
}
