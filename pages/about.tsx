import {
  Button,
  Container,
  Link,
  List,
  ListItem,
  Typography,
} from "@mui/material";

export default function AboutPage() {
  return (
    <Container sx={{ mt: "20svh" }}>
      <Typography variant="h2">About</Typography>
      <p>This software is in alpha and may be buggy</p>

      <Button
        href="https://toast.ooo/c/browse"
        target="_blank"
        variant="contained"
      >
        Lemmy Community
      </Button>

      <Typography variant="h3">Threads related to this project</Typography>
      <ol>
        <li>
          <b>!browse@toast.ooo</b>
          <Link
            href="https://toast.ooo/post/8335"
            target="_blank"
            sx={{ ml: 1 }}
          >
            View
          </Link>
        </li>
        <li>
          <b>!technology@beehaw.org</b>
          <Link
            href="https://toast.ooo/post/9908"
            target="_blank"
            sx={{ ml: 1 }}
          >
            View
          </Link>
        </li>
      </ol>

      <Typography variant="h3">Other Projects</Typography>
      <p>
        <Link href="https://lemmyverse.net" target="_blank">
          lemmyverse.net
        </Link>{" "}
        by{" "}
        <Link href="https://lemmy.tgxn.net/u/tgxn" target="_blank">
          @tgxn@lemmy.tgxn.net
        </Link>
      </p>
    </Container>
  );
}
