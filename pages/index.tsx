import { GetServerSideProps } from "next";

export default function Home() {
  return <div>home</div>;
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  return {
    redirect: {
      destination: "/communities",
    },
    props: {},
  };
};
