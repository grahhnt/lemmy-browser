import { AppProps } from "next/app";
import { BareFetcher, SWRConfig } from "swr";

import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import {
  AppBar,
  Box,
  Button,
  CssBaseline,
  ThemeProvider,
  Toolbar,
  Typography,
  createTheme,
} from "@mui/material";
import { swr_fetcher } from "@/lib/swrconfig";

export default function App({ Component, pageProps }: AppProps) {
  const dark = createTheme({
    palette: {
      mode: "dark",
    },
  });

  return (
    <SWRConfig
      value={{
        refreshInterval: 10000,
        fetcher: swr_fetcher,
      }}
    >
      <ThemeProvider theme={dark}>
        <CssBaseline />
        <AppBar>
          <Toolbar>
            <Typography variant="h6" component="div" sx={{ mr: 2 }}>
              Community Browser
            </Typography>
            <Box sx={{ flexGrow: 1, display: "flex" }}>
              <Button
                href="/communities"
                sx={{ my: 2, color: "white", display: "block" }}
              >
                Communities
              </Button>
              <Button
                href="/about"
                sx={{ my: 2, color: "white", display: "block" }}
              >
                About
              </Button>
            </Box>
          </Toolbar>
        </AppBar>
        <Component {...pageProps} />
      </ThemeProvider>
    </SWRConfig>
  );
}
