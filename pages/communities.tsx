import { ICommunity, IInstance, LemmyLanguage } from "@/lib/types";
import {
  Alert,
  AppBar,
  Avatar,
  Badge,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Chip,
  CircularProgress,
  Container,
  Grid,
  IconButton,
  Link,
  Stack,
  TextField,
  Toolbar,
  Tooltip,
  Typography,
} from "@mui/material";
import { useEffect, useMemo, useRef, useState } from "react";
import useSWR from "swr";
import useSWRInfinite from "swr/infinite";
import GroupIcon from "@mui/icons-material/Group";
import AddIcon from "@mui/icons-material/Add";
import TagIcon from "@mui/icons-material/Tag";
import LanguageIcon from "@mui/icons-material/Language";
import FilterListIcon from "@mui/icons-material/FilterList";
import { FilterDrawer } from "@/components/FilterDrawer";
import { useRouter } from "next/router";
import { LoadingButton } from "@mui/lab";
import { useInView } from "react-intersection-observer";

export default function Communities() {
  const router = useRouter();

  const [languages, setLanguages] = useState<LemmyLanguage[]>([]);
  const [homeserver, setHomeserver] = useState("toast.ooo");
  const [textFilter, _setTextFilter] = useState<string>("");
  const [filtered, _setFiltered] = useState<string[]>([]);
  const [showFilters, setShowFilters] = useState(false);
  const [loadMoreButtonRef, isLoadMoreVisible] = useInView();

  const swrKey = useMemo(
    () => (page: number) =>
      "/api/communities?" +
      [
        "limit=9",
        "page=" + page,
        textFilter && "search=" + textFilter,
        filtered.length > 0 && "filters=" + filtered,
      ]
        .filter((a) => a)
        .join("&"),
    [textFilter, filtered]
  );

  const {
    data: communityPages,
    isLoading: communitiesLoading,
    size: communitiesSize,
    setSize: setCommunitiesSize,
    error: communitiesError,
    isValidating: communitiesValidating,
  } = useSWRInfinite<{
    meta: { hasPrev: boolean; hasNext: boolean; total: number };
    communities: ICommunity[];
  }>(swrKey);

  useEffect(() => {
    if (isLoadMoreVisible) setCommunitiesSize(communitiesSize + 1);
    // ignore not included dependencies
    // @ts-ignore
  }, [isLoadMoreVisible]);

  useEffect(() => {
    fetch("/languages.json")
      .then((a) => a.json())
      .then((langs) => {
        setLanguages(langs);
      });
  }, []);

  useEffect(() => {
    if (router.query.search) {
      _setTextFilter(router.query.search as string);
    }
    if (router.query.filter) {
      _setFiltered((router.query.filter as string).split(","));
    }
  }, [router.query, _setFiltered]);

  const setFiltered = (filtered: string[]) => {
    _setFiltered(filtered);
    const { filter: _trash, ...other } = router.query;
    router.replace({
      query: {
        ...other,
        ...(filtered.length > 0 ? { filter: filtered.join(",") } : {}),
      },
    });
  };

  const setTextFilter = (text: string) => {
    _setTextFilter(text);
    const { search: _trash, ...other } = router.query;
    router.replace({
      query: {
        ...other,
        ...(text.length > 0 ? { search: text } : {}),
      },
    });
  };

  const getCommunityInstance = (
    community: ICommunity
  ): IInstance | undefined => {
    return undefined;
    // return instances?.find((i) => community._id.split("@")[1] === i.hostname);
  };

  const handleFollow = (community: ICommunity) => {
    alert("hi");
  };

  const getFollowHref = (community: ICommunity) =>
    homeserver
      ? `https://${homeserver}/search/q/${encodeURIComponent(
          "!" + community._id
        )}/type/Communities/sort/TopAll/listing_type/All/community_id/0/creator_id/0/page/1`
      : undefined;

  const toggleFilter = (filter: string) => {
    if (filtered.indexOf(filter) > -1) {
      setFiltered(filtered.filter((t) => t !== filter));
    } else {
      setFiltered([...filtered, filter]);
    }
  };

  return (
    <>
      <FilterDrawer
        languages={languages}
        open={showFilters}
        onClose={() => setShowFilters(false)}
        filtered={filtered}
        setFiltered={setFiltered}
      />
      <Box
        sx={{
          width: "100%",
          height: "30svh",
          position: "relative",
        }}
      >
        <Box
          sx={{
            width: "100%",
            height: "100%",
            backgroundImage: `url("https://unsplash.com/photos/QFY3Tv5_12M/download?ixid=M3wxMjA3fDB8MXxhbGx8fHx8fHx8fHwxNjg2NzA2MjE5fA&force=true&w=1920")`,
            backgroundPosition: "top",
            backgroundSize: "cover",
            maskImage: "linear-gradient(180deg, rgba(0,0,0,1), rgba(0,0,0,0))",
          }}
        />
      </Box>
      <Container>
        <Box sx={{ mb: 2 }}>
          <Typography variant="h3">Community Browser</Typography>
          <Stack direction="row" gap={1} alignItems="center">
            <Box>
              <Badge color="warning" badgeContent={filtered.length}>
                <Button
                  variant="contained"
                  startIcon={<FilterListIcon />}
                  onClick={() => setShowFilters(true)}
                >
                  Filters
                </Button>
              </Badge>
            </Box>
            <Box>
              <TextField
                label="Search"
                variant="outlined"
                value={textFilter}
                onChange={(e) => {
                  setTextFilter(e.currentTarget.value);
                }}
              />
            </Box>
            <Box>
              <Typography>{communityPages?.[0].meta.total} total</Typography>
            </Box>
            <Box>
              <Button onClick={() => setCommunitiesSize(communitiesSize + 1)}>
                more
              </Button>
            </Box>
          </Stack>
        </Box>
        {communitiesLoading && <Alert severity="info">Loading...</Alert>}
        <Grid container spacing={1}>
          {(communityPages || []).map((communities, pageNumber) =>
            communities.communities.map((community) => {
              let instance = getCommunityInstance(community);

              return (
                <Grid
                  item
                  xs={12}
                  md={4}
                  key={pageNumber + "_" + community._id}
                >
                  <Card sx={{ width: "100%" }}>
                    <CardHeader
                      avatar={
                        <Avatar src={community.icon || undefined}>
                          <GroupIcon />
                        </Avatar>
                      }
                      title={community.title}
                      subheader={
                        <Link
                          color="inherit"
                          href={community.actor}
                          target="_blank"
                          noWrap={true}
                        >
                          {community.actor}
                        </Link>
                      }
                      action={
                        <Tooltip title="Subscribe">
                          <Button
                            variant="contained"
                            sx={{ minWidth: "auto", p: "6px" }}
                            href={getFollowHref(community) as string}
                            target="_blank"
                            onClick={() => {
                              if (getFollowHref(community)) return;
                              handleFollow(community);
                            }}
                          >
                            <AddIcon />
                          </Button>
                        </Tooltip>
                      }
                      sx={{
                        ".MuiCardHeader-content": {
                          display: "block",
                          overflow: "hidden",
                        },
                      }}
                      titleTypographyProps={{
                        noWrap: true,
                        textOverflow: "ellipsis",
                      }}
                    />
                    <CardMedia
                      component="div"
                      sx={{
                        backgroundImage: `url("${
                          community.banner || community.icon
                        }")`,
                        backgroundColor: "#222",
                        height: "200px",
                        ...(community.banner
                          ? {
                              backgroundSize: "cover",
                              backgroundPosition: "center",
                            }
                          : {
                              backgroundPosition: "center",
                              backgroundRepeat: "repeat",
                            }),
                      }}
                    />
                    <CardContent>
                      <Box
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          width: "100%",
                          overflowX: "auto",
                          pb: 1,
                          gap: 1,
                          scrollbarWidth: "thin",
                        }}
                      >
                        {community.flags.feature && (
                          <Chip color="info" label="Pinned" />
                        )}
                        {community.flags.nsfw && (
                          <Chip color="error" label="NSFW" />
                        )}
                        <Chip
                          label={`${community.stats.subscribers} subscribers`}
                        />
                        <Chip label={`${community.stats.posts} posts`} />
                        {community.subreddits.map((subreddit) => (
                          <Chip
                            key={subreddit}
                            color="success"
                            label={subreddit}
                            onClick={() =>
                              toggleFilter("subreddit:" + subreddit)
                            }
                          />
                        ))}
                        {community.tags.map((tag) => (
                          <Chip
                            key={tag}
                            color="info"
                            sx={
                              filtered.indexOf(tag) > -1
                                ? { "background-color": "#0288d1" }
                                : {}
                            }
                            icon={<TagIcon />}
                            label={tag}
                            onClick={() => toggleFilter(tag)}
                          />
                        ))}
                        {community.languages
                          ?.filter((l) => l !== 0)
                          .slice(0, 3)
                          .map((lang) => (
                            <Chip
                              key={lang}
                              variant="outlined"
                              icon={<LanguageIcon />}
                              label={
                                languages.find((l) => l.id === lang)?.name ||
                                lang
                              }
                              onClick={() => toggleFilter(`lang:${lang}`)}
                            />
                          ))}
                        {community.languages &&
                          community.languages?.length > 3 && (
                            <Chip
                              variant="outlined"
                              icon={<LanguageIcon />}
                              label={`+${
                                community.languages!.length - 3
                              } others`}
                            />
                          )}
                      </Box>
                      <Typography variant="body2" color="text.secondary">
                        {community.description?.substring(0, 100) || (
                          <i>No Description Set</i>
                        )}
                      </Typography>
                    </CardContent>
                  </Card>
                </Grid>
              );
            })
          )}
          {communityPages?.[communityPages.length - 1].meta.hasNext && (
            <Grid item xs={12} sx={{ my: 2 }}>
              <LoadingButton
                loading={communitiesValidating || communitiesLoading}
                sx={{ width: "100%" }}
                onClick={() => setCommunitiesSize(communitiesSize + 1)}
                ref={loadMoreButtonRef}
              >
                Load More
              </LoadingButton>
            </Grid>
          )}
        </Grid>
      </Container>
    </>
  );
}
