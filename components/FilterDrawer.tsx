import {
  Box,
  Button,
  Chip,
  Collapse,
  DialogTitle,
  Drawer,
  FormControlLabel,
  ListItemButton,
  ListItemText,
  Paper,
  Stack,
  Switch,
  Typography,
} from "@mui/material";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import { useState } from "react";
import TagIcon from "@mui/icons-material/Tag";
import LanguageIcon from "@mui/icons-material/Language";
import useSWR from "swr";
import { LemmyLanguage } from "@/lib/types";

export const FilterDrawer = ({
  languages,
  open,
  onClose,
  filtered,
  setFiltered,
}: {
  languages?: LemmyLanguage[];
  open: boolean;
  onClose: () => void;
  filtered: string[];
  setFiltered: (v: string[]) => void;
}) => {
  const { data: availableFilters } = useSWR<{
    languages: {
      _id: number;
      uses: number;
    }[];
    tags: {
      _id: string;
      uses: number;
    }[];
  }>("/api/filters");
  const [showTags, setShowTags] = useState(true);
  const [showLangs, setShowLangs] = useState(false);

  const toggleFilter = (filter: string) => {
    if (filtered.indexOf(filter) > -1) {
      setFiltered(filtered.filter((t) => t !== filter));
    } else {
      setFiltered([...filtered, filter]);
    }
  };

  return (
    <Drawer anchor="right" open={open} onClose={onClose}>
      <DialogTitle>Filters</DialogTitle>
      <Stack sx={{ width: "min(300px, 100vw)", mx: 2 }} gap={2}>
        {filtered.length > 0 && (
          <Box>
            <Button onClick={() => setFiltered([])}>Clear Filters</Button>
          </Box>
        )}
        <Box>
          <FormControlLabel
            control={
              <Switch
                checked={filtered.indexOf("nsfw:show") > -1}
                onChange={(e) => toggleFilter("nsfw:show")}
              />
            }
            label="Show NSFW"
          />
        </Box>
        {filtered.indexOf("nsfw:show") > -1 && (
          <Box>
            <FormControlLabel
              control={
                <Switch
                  checked={filtered.indexOf("nsfw:only") > -1}
                  onChange={(e) => toggleFilter("nsfw:only")}
                />
              }
              label="Only NSFW"
            />
          </Box>
        )}
        {filtered.find((f) => f.indexOf("subreddit:") === 0) && (
          <Box>
            <Typography>Filtered Subreddits</Typography>
            {filtered
              .filter((f) => f.indexOf("subreddit:") === 0)
              .map((sub) => sub.split(":")[1])
              .map((sub) => (
                <Chip
                  key={sub}
                  color="success"
                  label={sub}
                  onDelete={() => toggleFilter(`subreddit:${sub}`)}
                />
              ))}
          </Box>
        )}
        <Paper elevation={5}>
          <ListItemButton onClick={() => setShowTags(!showTags)}>
            <ListItemText primary="Tags" />
            {showTags ? <ExpandLess /> : <ExpandMore />}
          </ListItemButton>
          <Collapse in={showTags} timeout="auto">
            <Box
              sx={{
                display: "flex",
                flexWrap: "wrap",
                alignItems: "center",
                width: "100%",
                p: 1,
                gap: 1,
                scrollbarWidth: "thin",
              }}
            >
              {availableFilters?.tags.map((tag) => (
                <Chip
                  key={tag._id}
                  color="info"
                  sx={
                    filtered.indexOf(tag._id) > -1
                      ? { "background-color": "#0288d1" }
                      : {}
                  }
                  icon={<TagIcon />}
                  label={tag._id + ` (${tag.uses})`}
                  onClick={() => toggleFilter(tag._id)}
                />
              ))}
            </Box>
          </Collapse>
        </Paper>

        <Paper elevation={5}>
          <ListItemButton onClick={() => setShowLangs(!showLangs)}>
            <ListItemText primary="Languages" />
            {showLangs ? <ExpandLess /> : <ExpandMore />}
          </ListItemButton>
          <Collapse in={showLangs} timeout="auto">
            <Box
              sx={{
                display: "flex",
                flexWrap: "wrap",
                alignItems: "center",
                width: "100%",
                p: 1,
                gap: 1,
                scrollbarWidth: "thin",
              }}
            >
              {availableFilters?.languages.map((lang) => (
                <Chip
                  key={lang._id}
                  variant="outlined"
                  sx={
                    filtered.indexOf(`lang:${lang._id}`) > -1
                      ? { "background-color": "#0288d1" }
                      : {}
                  }
                  icon={<LanguageIcon />}
                  label={
                    (languages?.find((l) => l.id === lang._id)?.name ||
                      lang._id) + ` (${lang.uses})`
                  }
                  onClick={() => toggleFilter(`lang:${lang._id}`)}
                />
              ))}
            </Box>
          </Collapse>
        </Paper>
      </Stack>
    </Drawer>
  );
};
